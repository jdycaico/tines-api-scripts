# Tines API Scripts

The original development goal was to use GitHub to store all Tines application data, however this was abandoned due to technical limitations at the company.

However, this set of scripts *was* successfully used  to migrate the contents of our Tines Cloud tenant into our on-prem tenant.

More of a scripting project than a full-fledged application.

## Usage
For getting data into or out of a Tines tenant:
* Use **tines_get.py** for quick exports of data
* * Useful in debugging as the API has more verbose entity data than seen in UI
* Use **tines_compare.py** to "diff" the contents of one tenant with another

## Notes
- `data/teams/TED/stories/Tines_Techniques` is included as a refrence, to demonstrate the directory structure of exported story data
